/*
	Aggregation

	Aggragate Methods
	1. Match
	2. Group
	3. Sort
	4. Project

	Aggragate pipelines

	Operations:
	1. Sum
	2. Max
	3. Min
	4. Average
*/

//MongoDB Aggragate
/*
	used to generate manipulate and perform operation to create filtered results that helps in analyzing data
	- compared to doing CRUD operation on our data, aggregation gives us access to manipulate, filter, and compute for results, providing us with information to make neccessary development decisions
	- aggregation in MongoDB are very felxible and you can from your own aggregation pipeline depending on the need of your application
*/

//Aggragate Methods

/*
	Match - "$match"
	-is used to pass the documents that meet the specified condition(s) to the next pipeline stage/aggregation process

	Syntax:
		{ $match: {field: value}}
*/

db.fruits.aggregate([
	{$match: {onSale: true}}
	]);

db.fruits.aggregate([{
	$match : {stock : {$gte: 20}}
}]);

//Group

/*
	Group - "$group"
	-is used to group elements together field-value pairs using the data from the grouped elements

	Syntax:
		{$group: {_id: "value", fieldResult: "valueResult"}}
*/

db.fruits.aggregate([{
	$group : { _id: "$supplier_id", total: {$sum: "$stock"} }
}]);

//Sort

/*
	Sort - "$sort"
	-can be used when changing the order of aggregated results
	-providing a value of -1 will sort the documents in descending order
	-providing a value of 1 will sort the documents in an ascending order

	Syntax:
		{ $sort {field: 1/-1}}
*/

db.fruits.aggregate([
	{ $match : { onSale: true} },
	{ $group : { _id: "$supplier_id", total : {$sum: "$stock"} } },
	{ $sort : {total: -1} }
]);

/*
	Project - "$project"
	-can be used when aggregating data to include or exclude fields from returned results

	Syntax:
		{$project : {field : 1/0}}
*/

db.fruits.aggregate([
	{$match : { onSale: true}},
	{$group : { _id: "$supplier_id", total : {$sum: "$stock"}}},
	{$project : {_id: 0}}
]);

db.fruits.aggregate([
	{$match : { onSale: true}},
	{$group : { _id: "$supplier_id", total : {$sum: "$stock"}}},
	{$sort: {total: 1}},
	{$project : {_id: 0}}
]);

/*
	Aggregation Pipeline
	-Each stage transforms the documents as they pass through the deadlines

	Syntax:

		db.collectionName.aggregate([
			{stageA},
			{stageB},
			{stageC}
		])
*/

//Operators
/*
	Sum - "$sum"
	-total of everything
*/

db.fruits.aggregate([
	{$group: { _id: "$supplier_id", total: {$sum: "$stock"}}}
]);

/*
	Max - "$max"
	- gets the highest value out of everything else
*/

db.fruits.aggregate([
	{$group : { _id: "$supplier_id", max_price : {$max: "$price"}}}
]);

/*
	Min - "$min"
	- gets the lowest  value out of everything
*/

db.fruits.aggregate([
	{$group : { _id: "$supplier_id", min_price : {$min: "$price"}}}
]);

/*
	Average - "$avg"
	-get the average of all the fields
*/
db.fruits.aggregate([
	{$group : { _id: "$supplier_id", avg_price : {$avg: "$price"}}}
]);
